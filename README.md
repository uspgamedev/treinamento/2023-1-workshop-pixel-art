# SOBRE O WORKSHOP
Workshop sobre os fundamentos de pixel art, oferecido pelo USPGameDev como parte do treinamento 2023.1

<br />

# TUTORIAIS
- [Saint11](https://saint11.org/)
- [Pixel Overload](https://www.youtube.com/@PixelOverloadChannel)
- [Saultoons](https://www.youtube.com/@saultoons)
- [Brandon James Greer](https://www.youtube.com/@BJGpixel)

<br />

# CRÉDITOS
Fonte da apresentação por Essssam. Pode ser encontrada [aqui](https://essssam.itch.io/pixel-ae).

Paleta de cores da apresentação por Star. Pode ser encontrada [aqui](https://lospec.com/palette-list/twilight-5).

Pixel arts da apresentação:

- Dungeon Crawler Pixel Art Asset Pack, por Anokolisa. Pode ser encontrado [aqui](https://anokolisa.itch.io/dungeon-crawler-pixel-art-asset-pack).

- DungeonTileset II, por 0x72. Pode ser encontrado [aqui](https://0x72.itch.io/dungeontileset-ii).

- Gothicvania Patreon's Collection, by ansimuz. Pode ser encontrado [aqui](https://ansimuz.itch.io/gothicvania-patreon-collection).

- [Grimoire: Ars Bellica](https://kazuo256.itch.io/grimoire-ars-bellica) e [Cleber's Dungeon](https://github.com/uspgamedev/clebdung) (pertencem ao USPGameDev)